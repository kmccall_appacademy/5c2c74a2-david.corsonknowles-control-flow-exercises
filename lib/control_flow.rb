# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  result = ""
  str.each_char { |c| result << c if c == c.upcase }
  result
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    str[(str.length / 2)]
  else
    str[(str.length / 2 - 1)..(str.length / 2)]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)

def num_vowels(str)
  arr = str.chars
  arr.map! { |c| VOWELS.include?(c.downcase) ? c = 1 : c = 0 }
  arr.reduce(:+)
end

def num_vowels1(str)
  str.downcase.scan(/[aeiou]/).size
end

def num_vowels2(str)
  result = 0
  str.chars.each do |c|
    if VOWELS.include?(c)
      result +=1
    end
  end
  result
end

def num_vowels3(str)
  str.count(aeiouAEIOU)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  fac = 1
  num.downto(1) do |i|
    fac *= i
  end
  fac
end

# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  arr.each_with_index do |entry, index|
    unless index == (arr.length - 1)
      result << entry << separator
    else
      result << entry
    end
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = ""
  downcase_str = str.downcase
  downcase_str.each_char
    .with_index {|c, i| i.even? ? result << c : result << c.upcase }
  result
end

def weirdcase2(str)
  result = ""
  arr = str.downcase.chars
  arr.each_with_index {|c, i| i.even? ? result << c : result << c.upcase }
  result
end

# Reverse all words of five or more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  arr = str.split(" ")
  arr.each_with_index do |e, index|
    arr[index] = e.reverse if e.length >= 5
  end
  arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz1(n)
  result = (1..n).to_a
  result.each_with_index do |num, idx|
    result[idx] = "fizz" if num % 3 == 0
    result[idx] = "buzz" if num % 5 == 0
    result[idx] = "fizzbuzz" if num % 15 == 0  # assigned last, overwrites
  end
  result
end

def fizzbuzz2(n)
  result = (1..n).to_a
  result.each_with_index do |num, idx|
    if num % 15 == 0
      result[idx] = "fizzbuzz"
    elsif num % 3 == 0
      result[idx] = "fizz"
    elsif num % 5 == 0
      result[idx] = "buzz"
    else
    end
  end
  result
end

class Fizzbuzz

  def self.factory(options)
    self.new
    @m = options[:fizz]
    @o = options[:buzz]
  end

  def self.fizzbuzz(n)
    result = (1..n).to_a
    result.each_with_index do |num, idx|
      result[idx] = "fizz" if num % @m == 0
      result[idx] = "buzz" if num % @o == 0
      result[idx] = "fizzbuzz" if num % (@m * @o) == 0  # assigned last, overwrites
    end
    result
  end
end

Fizzbuzz.factory(:fizz => 3, :buzz => 5)

def fizzbuzz(n)
  Fizzbuzz::fizzbuzz(n)
end

# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  result = []
  (arr.length - 1).downto(0) do |i|
    result << arr [i]
  end
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  return true if num == 2
  (num - 1).downto(2) do |i|
    return false if num % i == 0
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  (1..num).each do |i|
    if num % i == 0
      result << i
    end
  end
  result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |e| prime?(e) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end

# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = arr.select { |e| e.odd? }
  evens = arr.select { |e| e.even? }
  odds.length > evens.length ? evens[0] : odds[0]
end
